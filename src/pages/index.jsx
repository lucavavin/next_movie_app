import styles from "@/styles/Home.module.css";
import pic from "public/img/pexels-photo-2659629.webp";
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import  axios from "axios";

export async function getServerSideProps(ctx) {
    var config = {
        method: "get",
        url: "https://api.themoviedb.org/3/search/multi?api_key=fd2eb972f06120fdc3c06a7a744a9e72&query="+ctx.query.s,
    };
    const data = axios(config)
        .then((response) => {
            return response.data.results;
        })
        .catch(function (error) {
            console.log(error);
        });
    return {
        props: {
            data: await data,
        },
    };
}

export default function Home({ data }) {
    const router = useRouter();
    const [searchTerm, updateSearchTerm] = useState(router.query.s || ""); 

    const handleChangeSearch = (searchData) => {
        updateSearchTerm(searchData);
        router.push({
            pathname: "/",
            query: {
                s: searchData,
            },
        });
    };

    const loader = ({src, width, height, quality}) => {
        return `https://image.tmdb.org/t/p/w500${srx}?w${width}${height}`
    }


    return (
        <>
            <h1 className={styles.center}>Movie search</h1>
            <div>
                <input
                    type='text'
                    name='search'
                    id=''
                    defaultValue={searchTerm}
                    onChange={(e) => handleChangeSearch(e.target.value)}
                />
                <li className={styles.img}>
                    {data && data.map(media => {
                        console.log(media.backdrop_path)
                        if (media.backdrop_path) {
                        return (
                        <ul className={styles.img}>
                            <Image src={ + media.backdrop_path} alt='Logo' height='400' width='100'/>
                        </ul> )}
                    })}
                    
                    <ul className='img'>
                        <Image src={pic} alt='Logo' height='400' />
                    </ul>
                    <ul className='img'>
                        <Image src={pic} alt='Logo' height='400' />
                    </ul>
                </li>
            </div>
        </>
    );
}
